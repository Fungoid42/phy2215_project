// Metropolis-Hastings algorithm
use plotters::prelude::*;
use rand::prelude::*;
use rand_distr::{Normal, Distribution};


const STOP: usize = 100_000;
const BURN_IN_STOP: usize = 10_000;
const MAX_COUNT: usize = 2048;
//const C: [[f64; 3]; 3] = [[0.065, 0.036, -0.056],[0.036, 0.182, -0.078],[-0.056, -0.078, 0.110]];
const C_INV: [[f64; 3]; 3] = [[27.4859, 0.804696, 14.5634], [0.804696, 7.91679, 6.02338], [14.5634, 6.02338, 20.7761]];


// Probability density
fn prob_density(state: [f64; 3]) -> f64 {
  let mut sum = 0.0;
  for i in 0..3 {
  	sum += state[i]*(state[0]*C_INV[0][i] + state[1]*C_INV[1][i] + state[2]*C_INV[2][i]);
  }
  
  let e = std::f64::consts::E; // Euler's number
  
  e.powf(-0.5 * sum)
}

fn plot_scatter(title: String, x_desc: String, y_desc: String, data: &Vec<[f64; 2]>, extrema: [[isize; 2]; 2]) 
-> Result<(), Box<dyn std::error::Error>> {
  let file_name = format!("plotters-doc-data/{}.png", title);

  let root =
      BitMapBackend::new(&file_name, (1920, 1080)).into_drawing_area();

  root.fill(&WHITE)?;

  let areas = root.split_by_breakpoints([1600], [160]);

  let mut x_hist_ctx = ChartBuilder::on(&areas[0])
      .y_label_area_size(80)
      .build_ranged(extrema[0][0]..extrema[0][1], 0usize..MAX_COUNT)?;
  let mut y_hist_ctx = ChartBuilder::on(&areas[3])
      .x_label_area_size(80)
      .build_ranged(0usize..MAX_COUNT, extrema[1][0]..extrema[1][1])?;
  let mut scatter_ctx = ChartBuilder::on(&areas[2])
      .x_label_area_size(60)
      .y_label_area_size(60)
      .build_ranged((extrema[0][0] as f64 / 100.0)..(extrema[0][1] as f64 / 100.0), (extrema[1][0] as f64 / 100.0)..(extrema[1][1] as f64 / 100.0))?;
  scatter_ctx
      .configure_mesh()
      .x_desc(x_desc)
      .y_desc(y_desc)
      .axis_desc_style(("sans-serif", 25).into_font())
      .draw()?;
  scatter_ctx.draw_series(
      data[BURN_IN_STOP..STOP]
        .iter()
        .map(|[x, y]| Circle::new((*x, *y), 2, BLUE.filled())),
  )?;
  scatter_ctx.draw_series(
      data[0..BURN_IN_STOP]
        .iter()
        .map(|[x, y]| Circle::new((*x, *y), 2, RED.filled())),
  )?;
  let x_hist_burn_in = Histogram::vertical(&x_hist_ctx)
      .style(RED.filled())
      .margin(0)
      .data(
          data[0..BURN_IN_STOP]
            .iter()
            .map(|[x, _]| ((x * 100.0).round() as isize, 1usize)),
      );
  let y_hist_burn_in = Histogram::horizontal(&y_hist_ctx)
      .style(RED.filled())
      .margin(0)
      .data(
          data[0..BURN_IN_STOP]
            .iter()
            .map(|[_, y]| ((y * 100.0).round() as isize, 1usize)),
      );
  let x_hist = Histogram::vertical(&x_hist_ctx)
      .style(BLUE.filled())
      .margin(0)
      .data(
          data[BURN_IN_STOP..STOP]
            .iter()
            .map(|[x, _]| ((x * 100.0).round() as isize, 1usize)),
      );
  let y_hist = Histogram::horizontal(&y_hist_ctx)
      .style(BLUE.filled())
      .margin(0)
      .data(
          data[BURN_IN_STOP..STOP]
            .iter()
            .map(|[_, y]| ((y * 100.0).round() as isize, 1usize)),
      );
  x_hist_ctx.draw_series(x_hist)?;
  x_hist_ctx.draw_series(x_hist_burn_in)?;
  y_hist_ctx.draw_series(y_hist)?;
  y_hist_ctx.draw_series(y_hist_burn_in)?;


  Ok(())
}


fn plot_histogram(title: String, data: &Vec<f64>, extrema: [isize; 2])
-> Result<(), Box<dyn std::error::Error>> {
  let file_name = format!("plotters-doc-data/{}.png", title);

  let root =
    BitMapBackend::new(&file_name, (1920, 1080)).into_drawing_area();

  root.fill(&WHITE)?;

  let mut chart = ChartBuilder::on(&root)
      .x_label_area_size(35)
      .y_label_area_size(50)
      .margin(5)
      .caption(title, ("sans-serif", 50.0).into_font())
      .build_ranged(extrema[0]..extrema[1], 0usize..MAX_COUNT)?;

  chart
      .configure_mesh()
      .line_style_1(&WHITE.mix(0.3))
      .y_desc("Count")
      .x_desc("S_0")
      .x_label_formatter(&|x| format!("{:.2}", *x as f64 / 100.0))
      .x_label_offset(0)
      .axis_desc_style(("sans-serif", 15).into_font())
      .draw()?;



  let actual = Histogram::vertical(&chart)
        .style(BLUE.filled())
        .margin(3)
        .data(
            data[BURN_IN_STOP..STOP]
                .iter()
                .map(|x| ((x * 100.0).round() as isize, 1usize)),
        );

  chart.draw_series(actual)?;

  Ok(())
}

fn plot_line(title: String, y_desc: String, data: &Vec<f64>, extrema: [isize; 2])
-> Result<(), Box<dyn std::error::Error>> {
  let file_name = format!("plotters-doc-data/{}.png", title);

  let root =
      BitMapBackend::new(&file_name, (1920, 1080)).into_drawing_area();

  root.fill(&WHITE)?;

  let mut chart = ChartBuilder::on(&root)
      .margin(10)
      .caption(
          title,
          ("sans-serif", 40),
      )
      .set_label_area_size(LabelAreaPosition::Left, 60)
      .set_label_area_size(LabelAreaPosition::Right, 60)
      .set_label_area_size(LabelAreaPosition::Bottom, 40)
      .build_ranged(
          0..BURN_IN_STOP,
          extrema[0]..extrema[1],
      )?;

  chart
      .configure_mesh()
      .disable_x_mesh()
      .disable_y_mesh()
      .x_labels(30)
      .x_desc("Iteration number")
      .y_desc(y_desc)
      .y_label_formatter(&|x| format!("{:.2}", *x as f64 / 100.0))
      .draw()?;

  let mut i = 0;
  chart.draw_series(LineSeries::new(
      data[0..BURN_IN_STOP]
      .iter().map(|x| {i += 1; (i, (x * 100.0).round() as isize)}),
      &RED,
  ))?;


  Ok(())
}

fn plot_line_burn_in(title: String, y_desc: String, data: &Vec<f64>)
-> Result<(), Box<dyn std::error::Error>> {
  let file_name = format!("plotters-doc-data/{}.png", title);

  let root =
      BitMapBackend::new(&file_name, (1920, 1080)).into_drawing_area();

  root.fill(&WHITE)?;

  let mut chart = ChartBuilder::on(&root)
      .margin(10)
      .caption(
          title,
          ("sans-serif", 40),
      )
      .set_label_area_size(LabelAreaPosition::Left, 60)
      .set_label_area_size(LabelAreaPosition::Right, 60)
      .set_label_area_size(LabelAreaPosition::Bottom, 40)
      .build_ranged(
          0..BURN_IN_STOP,
          0..100_usize,
      )?;

  chart
      .configure_mesh()
      .disable_x_mesh()
      .disable_y_mesh()
      .x_labels(30)
      .x_desc("Iteration number")
      .y_desc(y_desc)
      .y_label_formatter(&|x| format!("{:.2}", *x as f64 / 100.0))
      .draw()?;

  let mut i = 0;
  chart.draw_series(LineSeries::new(
      data[0..(BURN_IN_STOP/16)]
      .iter().map(|x| {i += 1; (i*16, (x * 100.0).round() as usize)}),
      &RED,
  ))?;


  Ok(())
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
  // Initial state
  let mut current_state = [0.0, 0.0, 0.0];

  // Proposal distribution with parameters
  let mean = 0.0;
  let mut variance: f64 = 1.0/512.0;
  let mut variance_record: Vec<f64> = Vec::with_capacity(BURN_IN_STOP/16);

  let mut prop_dist_0 = Normal::new(mean, variance).unwrap();
  let mut prop_dist_1 = Normal::new(mean, variance).unwrap();
  let mut prop_dist_2 = Normal::new(mean, variance).unwrap();

  let mut s: Vec<[f64; 3]> = Vec::with_capacity(STOP);

  // For plotting
  let mut data_scatter: [Vec<[f64; 2]>; 3] = [Vec::with_capacity(STOP), Vec::with_capacity(STOP), Vec::with_capacity(STOP)];
  let mut data_histogram: [Vec<f64>; 3] = [Vec::with_capacity(STOP), Vec::with_capacity(STOP), Vec::with_capacity(STOP)];
  let mut extrema: [[isize; 2]; 3] = [[0, 0]; 3];

  let mut acceptance_rate: f64 = 0.0;
  let mut acceptance_rate_record: Vec<f64> = Vec::with_capacity(BURN_IN_STOP/16);

  // Iterate
  for i in 0..STOP {
  	// Random candidate
	  let s_0: f64 = prop_dist_0.sample(&mut rand::thread_rng());
	  let s_1: f64 = prop_dist_1.sample(&mut rand::thread_rng());
	  let s_2: f64 = prop_dist_2.sample(&mut rand::thread_rng());
	  let prop_state = [s_0, s_1, s_2];

	  // Acceptance probability
	  let a: f64 = 1.0_f64.min(prob_density(prop_state) / prob_density(current_state));

	  // Accept or reject
		let u: f64 = rand::thread_rng().gen();

		if u <= a { // Accept
      current_state = prop_state;

			s.push(current_state);

      // Center the proposal distribution on the new current_state
      prop_dist_0 = Normal::new(current_state[0], variance).unwrap();
      prop_dist_1 = Normal::new(current_state[1], variance).unwrap();
      prop_dist_2 = Normal::new(current_state[2], variance).unwrap();

      // Keeps track of the extrema
      for j in 0..3 {
        if ((current_state[j] * 100.0).round() as isize) < extrema[j][0] {
          extrema[j][0] = (current_state[j] * 100.0).round() as isize;
        }
        if ((current_state[j] * 100.0).round() as isize) > extrema[j][1] {
          extrema[j][1] = (current_state[j] * 100.0).round() as isize;
        }
      }

      if i < BURN_IN_STOP {
        acceptance_rate += 1.0/16.0;
      }
		} else { // Reject
			s.push(current_state);
		}

    // Collecting data for plotting
    data_scatter[0].push([s[i][0], s[i][1]]);
    data_scatter[1].push([s[i][1], s[i][2]]);
    data_scatter[2].push([s[i][2], s[i][0]]);

    data_histogram[0].push(s[i][0]);
    data_histogram[1].push(s[i][1]);
    data_histogram[2].push(s[i][2]);

    if (i < BURN_IN_STOP) && (i % 16 == 0) {
      //println!("acceptance_rate: {}, variance: {}", acceptance_rate, variance);
      
      if acceptance_rate > 0.7 {
        variance += 1.0/128.0;
      } else if acceptance_rate > 0.5 {
        variance += 1.0/256.0;
      } else if acceptance_rate > 0.4 {
        variance += 1.0/512.0;
      } else if acceptance_rate > 0.3 {
        variance += 1.0/1024.0;
      } else if acceptance_rate < 0.2 {
        variance -= 1.0/512.0;
      }

      variance_record.push(variance);
      acceptance_rate_record.push(acceptance_rate);
      acceptance_rate = 0.0;
    }
	}

  // Padding
  for i in 0..3 {
    extrema[i][0] = extrema[i][0] - 2;
    extrema[i][1] = extrema[i][1] + 2;
  }

  // Plotting the data scatter (with histograms)
  plot_scatter("s_1_over_s_0".to_string(), "s_0".to_string(), "s_1".to_string(), &data_scatter[0], [extrema[0], extrema[1]])?;
  plot_scatter("s_2_over_s_1".to_string(), "s_1".to_string(), "s_2".to_string(), &data_scatter[1], [extrema[1], extrema[2]])?;
  plot_scatter("s_0_over_s_2".to_string(), "s_2".to_string(), "s_0".to_string(), &data_scatter[2], [extrema[2], extrema[0]])?;
  
  // Plotting the histograms of s_0, s_1 and s_2
  plot_histogram("histogram_s_0".to_string(), &data_histogram[0], extrema[0])?;
  plot_histogram("histogram_s_1".to_string(), &data_histogram[1], extrema[1])?;
  plot_histogram("histogram_s_2".to_string(), &data_histogram[2], extrema[2])?;

  // Plotting the value of s_0, s_1 and s_2, as a function of iteration
  plot_line("value_of_s_0_during_burn_in".to_string(), "s_0".to_string(), &data_histogram[0], extrema[0])?;
  plot_line("value_of_s_1_during_burn_in".to_string(), "s_1".to_string(), &data_histogram[1], extrema[1])?;
  plot_line("value_of_s_2_during_burn_in".to_string(), "s_2".to_string(), &data_histogram[2], extrema[2])?;

  plot_line_burn_in("variance_of_the_proposition_distribution_during_burn_in".to_string(), "sigma^2".to_string(), &variance_record)?;


	Ok(())
}