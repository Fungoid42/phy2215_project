# PHY2215 - Thermodynamique
## Projet numérique - Partie 1

# Comment l'utiliser:
1) Installer rust sur votre ordinateur (https://www.rust-lang.org/).
2) Télécharger le project (avec git clone par exemple).
3) Dans le terminal, naviguer vers la racine du projet (le dossier phy2215_project/) et exécuter la commande:
~~~bash
cargo run --release
~~~
Le programme sera alors compilé (la première fois ça peut prendre plusieurs minutes) et exécuté.

Les différents paramètres du programme sont les constantes en majuscule au début du fichier source (src/main.rs):
~~~rust
const STOP: usize = 100_000;
const BURN_IN_STOP: usize = 10_000;
const MAX_COUNT: usize = 2048;
//const C: [[f64; 3]; 3] = [[0.065, 0.036, -0.056],[0.036, 0.182, -0.078],[-0.056, -0.078, 0.110]];
const C_INV: [[f64; 3]; 3] = [[27.4859, 0.804696, 14.5634], [0.804696, 7.91679, 6.02338], [14.5634, 6.02338, 20.7761]];
~~~

# Résultats
Les états obtenus lors du rodage sont générés en rouge et les autres sont en bleu.

## Histogramme de s_0
![Alt text](plotters-doc-data/histogram_s_0.png)

## Histogramme de s_1
![Alt text](plotters-doc-data/histogram_s_1.png)

## Histogramme de s_2
![Alt text](plotters-doc-data/histogram_s_2.png)

## s_0 contre s_2
![Alt text](plotters-doc-data/s_0_over_s_2.png)

## s_1 contre s_0
![Alt text](plotters-doc-data/s_1_over_s_0.png)

## s_2 contre s_1
![Alt text](plotters-doc-data/s_2_over_s_1.png)

## Ajustement de la variance lors du rodage
![Alt text](plotters-doc-data/variance_of_the_proposition_distribution_during_burn_in.png)